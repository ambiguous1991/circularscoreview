package com.ambiguous1991.circularscoreview;

import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Interpolator;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;

import java.util.Locale;

public class CircularScoreView extends View {
    private final Paint backgroundPaint;
    private final Paint progressPaint;
    private final Paint progressBackgroundPaint;
    private final Paint textPaint;

    private int COLOR_HIGH = Color.parseColor("#21d07a");
    private int COLOR_HIGH_BACKGROUND = Color.parseColor("#204529");
    private int COLOR_MEDIUM = Color.parseColor("#d2d531");
    private int COLOR_MEDIUM_BACKGROUND = Color.parseColor("#423d0f");
    private int COLOR_LOW = Color.parseColor("#db2360");
    private int COLOR_LOW_BACKGROUND = Color.parseColor("#571435");
    private int COLOR_BACKGROUND = Color.parseColor("#081c22");
    private int COLOR_TEXT = Color.parseColor("#FFFFFF");

    private int backgroundColor, progressColor, textColor;
    private boolean showPercentage;
    private float maxCount;
    private float minCount;
    private float maxFill;
    private int animationLength;

    private ValueAnimator mValueAnimator;

    private int strokeWidth;

    private float currentValue;

    private float progress;

    private RectF circleBounds;
    private float radius;

    private TimeInterpolator mInterpolator;

    public CircularScoreView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        TypedArray typedArray = context.obtainStyledAttributes(attributeSet, R.styleable.CircularScoreView);

        backgroundColor = typedArray.getColor(R.styleable.CircularScoreView_colorBackground, COLOR_BACKGROUND);
        progressColor = typedArray.getColor(R.styleable.CircularScoreView_colorProgress, COLOR_HIGH);
        showPercentage = typedArray.getBoolean(R.styleable.CircularScoreView_showPercentage, false);
        textColor = typedArray.getColor(R.styleable.CircularScoreView_textColor, COLOR_TEXT);
        strokeWidth = typedArray.getInt(R.styleable.CircularScoreView_strokeWidth, 50);
        maxCount = typedArray.getFloat(R.styleable.CircularScoreView_maxValue, 10f);
        minCount = typedArray.getFloat(R.styleable.CircularScoreView_minValue, 0f);
        animationLength = typedArray.getInt(R.styleable.CircularScoreView_animationLenght, 1000);
        maxFill = typedArray.getFloat(R.styleable.CircularScoreView_maxFill, maxCount);

        typedArray.recycle();

        circleBounds = new RectF();

        backgroundPaint = new Paint();
        backgroundPaint.setStyle(Paint.Style.FILL);
        backgroundPaint.setAntiAlias(true);
        backgroundPaint.setStrokeCap(Paint.Cap.SQUARE);
        backgroundPaint.setColor(backgroundColor);

        progressPaint = new Paint();
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setAntiAlias(true);
        progressPaint.setStrokeWidth(strokeWidth);
        progressPaint.setStrokeCap(Paint.Cap.ROUND);
        progressPaint.setColor(progressColor);

        textPaint = new TextPaint();
        textPaint.setColor(textColor);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        progressBackgroundPaint = new Paint();
        progressBackgroundPaint.setStyle(Paint.Style.STROKE);
        progressBackgroundPaint.setAntiAlias(true);
        progressBackgroundPaint.setStrokeWidth(strokeWidth);
        progressBackgroundPaint.setColor(COLOR_HIGH_BACKGROUND);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        float centerWidth = canvas.getWidth() / 2;
        float centerHeight = canvas.getHeight() / 2;

        radius = canvas.getWidth()/2 - (float)(0.1*canvas.getWidth());

        progressPaint.setStrokeWidth(radius/6);
        progressBackgroundPaint.setStrokeWidth(radius/6);

        float distanceFromBorder = 0.8f * radius;

        // set bound of our circle in the middle of the view
        circleBounds.set(centerWidth - distanceFromBorder,
                centerHeight - distanceFromBorder,
                centerWidth + distanceFromBorder,
                centerHeight + distanceFromBorder);

        // draw background circle
        canvas.drawCircle(centerWidth, centerHeight, radius, backgroundPaint);

        canvas.drawArc(circleBounds, 0, 360, false, progressBackgroundPaint);

        canvas.drawArc(circleBounds, -90, (float) (progress*(maxFill/maxCount) * 360), false, progressPaint);

        textPaint.setTextSize(radius*0.55f);

        if(showPercentage) {
            canvas.drawText(String.format(Locale.ENGLISH,"%.0f", currentValue)+"%",
                    centerWidth,
                    centerHeight + (textPaint.getFontMetrics().bottom),
                    textPaint);
        }
        else if (currentValue==Math.round(currentValue)){
            canvas.drawText(String.format(Locale.ENGLISH,"%.0f", currentValue),
                    centerWidth,
                    centerHeight + (textPaint.getFontMetrics().bottom),
                    textPaint);
        }
        else{
            canvas.drawText(String.format(Locale.ENGLISH,"%.1f", currentValue),
                    centerWidth,
                    centerHeight + (textPaint.getFontMetrics().bottom),
                    textPaint);
        }
    }

    public void start(){
        mValueAnimator = ValueAnimator.ofFloat(0f,1f);
        mValueAnimator.setDuration(animationLength);
        if(mInterpolator==null) {
            mValueAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        }
        else
            mValueAnimator.setInterpolator(mInterpolator);
        mValueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                currentValue = (maxFill * mValueAnimator.getAnimatedFraction());
                progress = mValueAnimator.getAnimatedFraction();

                invalidate();
            }
        });
        mValueAnimator.start();
    }

    public void setLinearInterpolation(){
        mInterpolator = new LinearInterpolator();
    }

    public void setAccelerateDecelerateInterpolator(){
        mInterpolator = new AccelerateDecelerateInterpolator();
    }

    public void setColorHigh(){
        progressPaint.setColor(COLOR_HIGH);
        progressBackgroundPaint.setColor(COLOR_HIGH_BACKGROUND);
    }

    public void setColorMedium(){
        progressPaint.setColor(COLOR_MEDIUM);
        progressBackgroundPaint.setColor(COLOR_MEDIUM_BACKGROUND);
    }

    public void setColorLow(){
        progressPaint.setColor(COLOR_LOW);
        progressBackgroundPaint.setColor(COLOR_LOW_BACKGROUND);
    }

    public void setMaxValue(float value){
        maxFill = value;
    }
}
