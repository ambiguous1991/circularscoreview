package com.ambiguous1991.circularscoreviewexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.ambiguous1991.circularscoreview.CircularScoreView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CircularScoreView csv = findViewById(R.id.circularScoreView);
        csv.setColorHigh();
        csv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CircularScoreView cs = (CircularScoreView) v;
                cs.setMaxValue(7);
                cs.start();
            }
        });
    }
}
